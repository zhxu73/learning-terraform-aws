
variable "region" {
  type = string
  description = "string, AWS region name; default = us-east-1"
  default = "us-east-1"
}

variable "vpc_name" {
  type = string
  description = "string, Name of the vpc; default = tf-example"
  default = "tf-example"
}

