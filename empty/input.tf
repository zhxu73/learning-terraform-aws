
variable "region" {
  type = string
  description = "string, AWS region name; default = us-east-1"
  default = "us-east-1"
}

variable "username" {
  type = string
  description = "string, username"
}

